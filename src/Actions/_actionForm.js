import {
    MOSTRAR_FORM,
} from '../Constantes';

//Mostrar formulario
export function mostrarFormAction(payload) {
    return (dispatch) => {
        dispatch(mostrarForm(payload));
    }
}

const mostrarForm = payload => ({
    type: MOSTRAR_FORM,
    payload: payload
});