import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid, Typography, Paper, StepLabel, Divider
} from '@material-ui/core'
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles((theme) => ({
    [theme.breakpoints.down('sm')]: {
        sizeType: {
            fontsize: 11
        },          
    },
    [theme.breakpoints.up('md')]: {
        height: 300,
        width: 500,
        marginBottom: '50%'
    },
    [theme.breakpoints.up('lg')]: {
        height: 300,
        width: 600,
        marginBottom: 200
    },
}));

const Partes = () => {
    const classes = useStyles();
    const [selectedValue, setSelectedValue] = useState('a');
    const openForm = useSelector(state => state._reducerForm.openForm);

    const handleChange = (event) => {
        setSelectedValue(event.target.value);
    };

    return (
        <Grid container style={{ padding: 20 }}>
            <Grid xs={6} sm={6} md={6}>
                <Typography>
                    Tamaño de la {openForm}
                </Typography>
            </Grid>
            <Grid xs={6} sm={6} md={6} style={{ marginTop: -10 }}>
                <RadioGroup row  >
                    <FormControlLabel
                        value="a"
                        onChange={handleChange}
                        control={<Radio color="secondary" checked={selectedValue === 'a'}/>}
                        label={<pan className={classes.sizeType}>S</pan>}
                        labelPlacement="start"
                    />
                    <FormControlLabel
                        value="b"
                        control={<Radio color="secondary" checked={selectedValue === 'b'} size='small' />}
                        onChange={handleChange}
                        label={<pan className={classes.sizeType}>M</pan>}
                        labelPlacement="start"
                    />
                    <FormControlLabel
                        value="c"
                        control={<Radio color="secondary" checked={selectedValue === 'c'} size='small'/>}
                        onChange={handleChange}
                        label={<pan className={classes.sizeType}>L</pan>}
                        labelPlacement="start"
                    />
                </RadioGroup>
            </Grid>
        </Grid>
    );
}

export default Partes;