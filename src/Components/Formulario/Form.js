import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
  Grid, Typography, Paper, StepLabel, Divider
} from '@material-ui/core'
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import clsx from 'clsx';
import StepButton from '@material-ui/core/StepButton';
import StepConnector from '@material-ui/core/StepConnector';
import car1 from '../../img/SedanPart.png';
import car2 from '../../img/HatchBackPart.png';
import car3 from '../../img/SuvPart.png';
import car4 from '../../img/PickUpPart.png';
import uno from '../../img/1.png';
import dos from '../../img/2.png';
import tres from '../../img/3.png';

/*Componentes */
import Partes from './Part/Partes';

const ColorlibConnector = withStyles({
  line: {
    border: 0,
  },
})(StepConnector);

const useQontoStepIconStyles = makeStyles((theme) => ({

  [theme.breakpoints.down('sm')]: {
    padding: {
      paddingLeft: 10,
      paddingRight: 10
    },
  },
  [theme.breakpoints.up('md')]: {
    padding: {
      paddingLeft: 40,
      paddingRight: 40
    },
  },
  [theme.breakpoints.up('lg')]: {
    padding: {
      paddingLeft: 40,
      paddingRight: 40
    },
  }


}));

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active } = props;

  const icons = {
    1: <img src={uno} alt='' />,
    2: <img src={dos} alt='' />,
    3: <img src={tres} alt='' />,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}


const useColorlibStepIconStyles = makeStyles((theme) => ({

  [theme.breakpoints.down('sm')]: {
    root: {
      backgroundColor: '#BBBBBB',
      zIndex: 1,
      color: '#fff',
      width: 50,
      height: 50,
      display: 'flex',
      borderRadius: '50%',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: -43,
    },
    active: {
      backgroundColor: '#AA221D',
      boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
    },
  },
  [theme.breakpoints.up('md')]: {
    root: {
      backgroundColor: '#BBBBBB',
      zIndex: 1,
      color: '#fff',
      width: 65,
      height: 65,
      display: 'flex',
      borderRadius: '50%',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: -53
    },
    active: {
      backgroundColor: '#AA221D',
      boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
    },
  },
  [theme.breakpoints.up('lg')]: {
    root: {
      backgroundColor: '#BBBBBB',
      zIndex: 1,
      color: '#fff',
      width: 75,
      height: 75,
      display: 'flex',
      borderRadius: '50%',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: -63
    },
    active: {
      backgroundColor: '#AA221D',
      boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
    },
  }


}));

function getStepContent(step) {
  switch (step) {
    case 0:
      return <Partes />;
    case 1:
      return 'Step 2: What is an ad group anyways?';
    case 2:
      return 'Step 3: This is the bit I really care about!';
    default:
      return 'Unknown step';
  }
}

function getSteps() {
  return ['Selecciona las partes', 'Tipos de servicio', 'Cotiza tu auto'];
}


const Form = () => {

  const classes = useQontoStepIconStyles();
  const history = useHistory();
  const [activeStep, setActiveStep] = useState(0);

  const steps = getSteps();

  const openForm = useSelector(state => state._reducerForm.openForm);

  useEffect(() => {
    if (openForm === undefined) {
      history.push('/')
    }
  });

  const handleStep = (step) => () => {
    setActiveStep(step);
  };



  return (

    <Grid>
      <Grid container>
        <Grid xs={12} sm={12} md={12} >
          <Typography color='primary' align='center' variant='h4'>
            Seleccione los paños en cada círculo de la imagen
          </Typography>
        </Grid>

        <Grid container style={{ marginTop: 40, padding: 0 }} >
          <Grid item xs={12} sm={5} md={5} className={classes.padding}>
            <Paper elevation={4} style={{ borderRadius: '8px' }}>
              <Stepper alternativeLabel nonLinear activeStep={activeStep} style={{ borderRadius: '8px' }}
                connector={<ColorlibConnector />}>
                {steps.map((label, index) => {
                  return (
                    <Step key={label}>
                      <StepButton
                        onClick={handleStep(index)}
                      >
                        <StepLabel StepIconComponent={ColorlibStepIcon} >
                          <Typography >{label}</Typography>
                        </StepLabel>
                      </StepButton>
                    </Step>

                  );
                })}
              </Stepper>
              <Divider style={{ marginLeft: 40, marginRight: 40, marginBottom: 20 }} />
              <div>
                <Typography >{getStepContent(activeStep)}</Typography>
              </div>
            </Paper>
          </Grid>
          <Grid xs={12} sm={7} md={7} >
            <Grid xs={6} sm={6} md={6} className='form__Part'>
              {openForm === 'SUV' ?
                <img src={car3} alt='img' />
                :
                openForm === 'Pick up' ?
                  <img src={car4} alt='img' />
                  :
                  openForm === 'Sedan' ?
                    <img src={car1} alt='img' />
                    :
                    <img src={car2} alt='img' />
              }
            </Grid>
            <Grid xs={6} sm={6} md={6} ></Grid>


          </Grid>
        </Grid>
      </Grid>
    </Grid >
  );
}

export default Form;