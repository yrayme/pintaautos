import React from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import {
    Grid, Typography, Card, CardContent, Container, 
} from '@material-ui/core'
import hr from '../../img/hr.png'
import car from '../../img/loading.png'


/*Actions */
import { mostrarFormAction } from '../../Actions/_actionForm';

/**Constantes */
import { CARS } from '../../Constantes'

import '../css/index.css';


const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(1),
        [theme.breakpoints.down('sm')]: {
            height: 200,
            width: 250,
            marginBottom: '50%'
        },
        [theme.breakpoints.up('md')]: {
            height: 300,
            width: 500,
            marginBottom: '50%'
        },
        [theme.breakpoints.up('lg')]: {
            height: 300,
            width: 600,
            marginBottom: 200
        },
    },

    typography: {
        padding: theme.spacing(1),
        [theme.breakpoints.down('sm')]: {
            fontSize: 31,
            color: 'white',
            marginTop: '50%'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: 31,
            color: 'white',
            marginTop: '50%'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: 31,
            color: 'white',
            marginTop: 220
        },
    },

    typography2: {
        padding: theme.spacing(1),
        [theme.breakpoints.down('sm')]: {
            fontSize: 28,
            color: 'white',
            position: 'absolute',
            bottom: '56%'
        },
        [theme.breakpoints.up('md')]: {
            fontSize: 28,
            color: 'white',
            position: 'absolute',
            bottom: '56%'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: 28,
            color: 'white',
            position: 'absolute',
            bottom: '60%'
        },
    },
}));



const Cards = () => {

    const classes = useStyles();
    //utilizar use dispatch y te crea una función
    const dispatch = useDispatch();
    const history = useHistory();

    const card = (event) => {
        let enlaces = document.getElementById('enlaces');
        let contador = 0;

        if (contador === 0) {
            enlaces.className = ('enlaces dos');
            contador = 1;
        }

        dispatch(mostrarFormAction(event))
        setTimeout(() => history.push('/Form'), 3000)

    }

    return (
        <div >
            <Container maxWidth='md'>
                <div className="enlaces uno" id='enlaces' justify='center'>
                    <Typography align='center' className={classes.typography}>Estas a un paso renovar tu auto</Typography>
                    <Typography align='center' className={classes.typography2} >¡Quedará como nuevo!</Typography>
                    <img src={car} alt='' className={classes.root} />
                </div>
                <Grid container align="center" >
                    <Grid xs={12} sm={12} md={12} >
                        <Typography color='primary' align='center' variant='h4'>
                            Elige el tipo de su vehículo
                        </Typography>
                        <img src={hr} alt="" /><br />
                    </Grid>
                    <Grid container spacing={2} style={{ width: '100%', marginTop: 20 }}
                        alignItems="center">
                        {CARS.map(car => (
                            <Grid xs={12} sm={6} md={6} container style={{ padding: 20 }} align='center' >

                                <Card variant="elevated" elevation={5} align='center'
                                    class={'card__body'}
                                    onClick={(event) => { card(car.title) }}
                                >
                                    <Typography variant="h5" component="h2" color='secondary'
                                        style={{ marginTop: 10, marginBottom: 20, fontWeight: 'bold' }}>
                                        {car.title}
                                    </Typography>

                                    <CardContent>
                                        <img src={car.img} alt="" className='card__img' /><br />
                                        <Typography
                                            className={'card__chip'}
                                            component='p'
                                            style={{ padding: 3, marginTop: 15 }}
                                        >
                                            {car.price} {car.amount}
                                        </Typography>
                                    </CardContent>
                                </Card>

                            </Grid>
                        ))}
                    </Grid>

                    <Grid xs={12} sm={12} md={12} align='center' style={{ marginBottom: 20 }} >
                        <Typography
                            className={'Chip'}
                            style={{ padding: 5, marginTop: 15, color: 'white' }}
                        >
                            El precio del planchado dependerá del daño del vehículo.
                            * Precios incluyen IGV
                        </Typography>
                    </Grid>
                </Grid>
                {/* {color && 'prueba'} */}
            </Container>
            {/* } */}
        </div>
    );
}

export default Cards;