import React from 'react';
import { Grid, Typography } from '@material-ui/core'
import '../css/index.css';
import Facebook from '../../img/facebook.png';
import Instagram from '../../img/instagram.png';

const Footer = () => {
    return (

        <Grid container style={{ padding: 20 }}>
            <Grid xs={6} sm={6} md={6} >
                <Typography color='primary' align='right' style={{ fontSize: '20px' }}>
                    Síguenos:
                </Typography>

            </Grid>
            <Grid xs={3} sm={2} md={1} align="left" style={{ marginTop: -5, marginLeft: 7 }}>
                <Typography
                    className='footer__icon'
                    align='center'
                >
                    <img src={Facebook} alt="" style={{ marginTop: 9 }} />
                </Typography>
            </Grid>
            <Grid xs={3} sm={2} md={1} align='left' className='footer__instagram'>
                <Typography
                    className='footer__icon'
                    align='center'
                >
                    <img src={Instagram} alt="" style={{ marginTop: 9 }} />
                </Typography>
            </Grid>
            <Grid sm={2} md={4} align='left'>
            </Grid>
            <Grid xs={12} sm={6} md={6} style={{ marginTop: 20 }}>
                <Typography color='primary' className='footer__copy'>
                    Copyright © Ampay MKT S.A.C. Todos los derechos reservados.
                </Typography>
            </Grid>
            <Grid xs={12} sm={6} md={6} style={{ marginTop: 20 }}>
                <Typography color='primary' className='footer__term'>
                    Términos y condiciones.
                </Typography>
            </Grid>
        </Grid>
    );
}

export default Footer;