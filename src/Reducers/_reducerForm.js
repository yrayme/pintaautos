import {
    MOSTRAR_FORM,
} from '../Constantes';

//cada reducer tiene su state

// const initialState = {
//     alerta: null
// }

export default function (state = {}, action) {
    switch (action.type) {
        case MOSTRAR_FORM:
            return {
                ...state,
                openForm: action.payload,
            }
        // case OCULTAR_ALERTA:
        //     return {
        //         ...state,
        //         alerta: null,
        //     }
        default:
            return state;
    }
}