
import './App.css';
import Logo from './img/logo.png';
import { ThemeProvider } from '@material-ui/styles';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

/*Components */
import Footer from './Components/Footer/Footer';
import Cards from './Components/Card/Cards';
import Form from './Components/Formulario/Form';

/*Constantes */
import { CAR_THEME } from './Constantes'


//Redux
import { Provider } from 'react-redux';
import store from './store/store';
import { Link } from 'react-router-dom';
// import { createMuiTheme } from '@material-ui/core/styles';

const theme = CAR_THEME;


function App() {
  

  return (

    <Router>
      <Provider store={store}>
        <ThemeProvider theme={theme} className="App">
          <div className="App" >
            <header className='App-header'> 
            <Link to={'/'} aling='center'>
            <img src={Logo} className="App-header__logo " alt="logo" />
                </Link>
             
            </header>
            <Switch>
            <Route exact path='/' component={Cards} />
            <Route exact path='/Form' component={Form} />
            </Switch>
            {/* <Cards /> */}
            <Footer />
          </div>

        </ThemeProvider>
      </Provider>
    </Router>
  );
}

export default App;
