import { createMuiTheme } from '@material-ui/core/styles';
import car1 from '../img/Pickup.png'
import car2 from '../img/SUV.png';
import car3 from '../img/sedan.png';
import car4 from '../img/HatchBack.png';

/* Digitel Feature constants */
export const CAR_THEME = createMuiTheme({
  typography: {
    fontFamily: 'Jost',
    fontStyle: 'normal'
  },
  palette: {
    primary: {
      main: '#9E9D9D',
    },
    secondary: {
      main: '#AA221D',
    }
  },
  

});

export const CARS = [
  {
    img: car1,
    title: 'Pick up',
    price: 'Precio por año desde: ',
    amount: <strong>S/229</strong>
  },
  {
    img: car4,
    title: 'SUV',
    price: 'Precio por año desde: ',
    amount: <strong>S - S/199 / M - S/209 / S - S/229</strong>
  },
  {
    img: car3,
    title: 'Sedan',
    price: 'Precio por año desde:', 
    amount: <strong>S/189</strong>
  },
  {
    img: car2,
    title: 'Hatchback',
    price: 'Precio por año desde:',
    amount: <strong>S/179</strong>
  },
]

export const MOSTRAR_FORM = 'MOSTRAR_FORM';

